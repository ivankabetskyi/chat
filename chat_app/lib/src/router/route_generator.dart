import 'package:chat_app/src/pages/Chat/chat_page.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Text('main page'));
      case '/chat':
        return MaterialPageRoute(builder: (_) => ChatPage());
      default:
        return MaterialPageRoute(builder: (_) => Text('Error'));
    }
  }
}
