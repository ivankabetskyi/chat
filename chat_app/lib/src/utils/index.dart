import 'package:flutter/material.dart';

Color getColorFromHEX(String hex) {
  final buffer = StringBuffer();
  buffer.write(hex.replaceFirst('#', 'ff'));
  int color = int.parse(buffer.toString(), radix: 16);
  return Color(color);
}

void clickOutsideTextFeild(BuildContext context) {
  FocusScope.of(context).unfocus();
}
