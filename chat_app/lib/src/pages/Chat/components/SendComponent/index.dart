import 'package:chat_app/src/pages/Chat/components/common/ActionButton/index.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

class SendComponent extends StatefulWidget {
  _SendState createState() => new _SendState();
}

class _SendState extends State<SendComponent> {
  TextEditingController _messageControllerListener;

  @override
  void initState() {
    _messageControllerListener = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      height: 40,
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: TextField(
              controller: _messageControllerListener,
              maxLines: 1,
              textAlignVertical: const TextAlignVertical(y: 1),
              keyboardType: TextInputType.text,
              style: TextStyle(height: 1, color: Colors.black87, fontSize: 14),
              decoration: InputDecoration(
                hintText: 'Message...',
                hintStyle:
                    TextStyle(color: getColorFromHEX('#ADB1C2'), fontSize: 14),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    borderSide: BorderSide(color: Colors.white)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    borderSide: BorderSide(color: Colors.white54)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    borderSide: BorderSide(color: Colors.white)),
                fillColor: getColorFromHEX('#F5F6FA'),
                filled: true,
              ),
            ),
          ),
          SizedBox(
            width: 40.0,
            child: Container(
              alignment: Alignment.center,
              child: ActionButton(
                icon: Icons.send,
                iconColor: getColorFromHEX('#2042FF'),
                size: 24.0,
              ),
            ),
          )
        ],
      ),
    );
  }
}
