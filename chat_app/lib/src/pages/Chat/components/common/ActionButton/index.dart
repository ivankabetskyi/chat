import 'package:chat_app/src/pages/Chat/models/SizeActionData.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:chat_app/src/pages/Chat/utils/index.dart';
import 'package:flutter/material.dart';

import 'utils/index.dart';

class ActionButton extends StatefulWidget {
  final IconData icon;
  final Color iconColor;
  final double size;
  ActionButton(
      {Key key,
      @required this.icon,
      @required this.iconColor,
      @required this.size})
      : super(key: key);
  @override
  _ActionButtonState createState() => new _ActionButtonState();
}

class _ActionButtonState extends State<ActionButton> {
  SizeActionData buttonSize;
  SizeActionData initialButtonSize;
  @override
  void initState() {
    initialButtonSize = getInitialActionSize(widget.size);
    buttonSize = initialButtonSize;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: sendMessage,
      child: TweenAnimationBuilder(
        tween: Tween<double>(begin: buttonSize.begin, end: buttonSize.end),
        duration: Duration(milliseconds: 300),
        builder: animationBuilder,
        onEnd: handleEndAnimation,
      ),
    );
  }

  void sendMessage() {
    setState(() {
      buttonSize = getDataForActionButton(widget.size);
    });
    clickOutsideTextFeild(context);
    print('click button');
  }

  void handleEndAnimation() {
    setState(() {
      buttonSize = initialButtonSize;
    });
  }

  Widget animationBuilder(_, double size, __) {
    return Icon(
      widget.icon,
      color: widget.iconColor,
      size: size,
    );
  }
}
