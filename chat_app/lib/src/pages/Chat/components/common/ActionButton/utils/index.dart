import 'package:chat_app/src/pages/Chat/models/SizeActionData.dart';

SizeActionData getDataForActionButton(double size) {
  double updatedSize = size * .8;
  return new SizeActionData(begin: size, end: updatedSize);
}
