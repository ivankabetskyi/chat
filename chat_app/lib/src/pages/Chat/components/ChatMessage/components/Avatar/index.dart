import 'package:chat_app/src/app/models/UserData/index.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final UserData sender;
  Avatar({Key key, this.sender}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Icon(
          sender.avatar,
          color: getColorFromHEX('#F5F6FA'),
          size: 40.0,
        ),
        sender.isOnline
            ? Positioned(
                bottom: 3,
                right: 3,
                child: Container(
                  height: 14,
                  width: 14,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: getColorFromHEX('#08D863')),
                    height: 10.0,
                    width: 10.0,
                  ),
                ),
              )
            : SizedBox(),
      ],
    );
  }
}
