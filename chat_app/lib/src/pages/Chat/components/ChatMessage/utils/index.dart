import 'package:chat_app/src/app/models/UserData/index.dart';
import 'package:intl/intl.dart';

String getFullUserName(UserData user) => '${user.name} ${user.surname}';

bool isMessageSentToday(DateTime date) {
  DateTime dateNow = DateTime.now();
  if (dateNow.day != date.day) {
    return false;
  }

  if (dateNow.month != date.month) {
    return false;
  }

  if (dateNow.year != date.year) {
    return false;
  }

  return true;
}

String getTimeFromCreatedMessage(DateTime date) {
  String sendTime = isMessageSentToday(date)
      ? DateFormat.Hm().format(date)
      : '${DateFormat.yMMMM().format(date)} ${DateFormat.Hm().format(date)}';

  return sendTime;
}
