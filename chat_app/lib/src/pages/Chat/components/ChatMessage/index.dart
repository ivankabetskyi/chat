import 'package:chat_app/src/pages/Chat/components/ChatMessage/components/Avatar/index.dart';
import 'package:chat_app/src/pages/Chat/components/ChatMessage/utils/index.dart';
import 'package:chat_app/src/pages/Chat/models/MessageData.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

class ChatMessage extends StatelessWidget {
  final MessageData message;
  final BorderRadius borderRadius;
  final MainAxisAlignment mainAxisAlignment;
  final Color backgroundColor;
  final Color textColor;
  final bool isCurrentUser;

  ChatMessage(
      {Key key,
      @required this.message,
      @required this.borderRadius,
      @required this.mainAxisAlignment,
      @required this.backgroundColor,
      @required this.textColor,
      @required this.isCurrentUser})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        children: [
          isCurrentUser
              ? SizedBox()
              : Row(
                  children: [
                    Avatar(
                      sender: message.sender,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          getFullUserName(message.sender),
                          style: TextStyle(
                              color: getColorFromHEX('#8387A0'),
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0),
                        ),
                        Text(
                          getTimeFromCreatedMessage(message.createdAt),
                          style: TextStyle(
                              color: getColorFromHEX('#CFD2E0'),
                              fontSize: 12.0),
                        )
                      ],
                    )
                  ],
                ),
          Row(
            mainAxisAlignment: mainAxisAlignment,
            children: [
              Container(
                constraints: BoxConstraints(maxWidth: 300.0),
                padding: EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                    color: backgroundColor, borderRadius: borderRadius),
                child: Text(
                  message.message,
                  style: TextStyle(
                      fontSize: 16.0, fontFamily: 'Roboto', color: textColor),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
