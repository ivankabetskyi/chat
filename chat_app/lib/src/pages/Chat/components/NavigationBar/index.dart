import 'package:chat_app/src/pages/Chat/components/NavigationBar/components/InfoTitle/index.dart';
import 'package:chat_app/src/pages/Chat/components/NavigationBar/components/UserIcon/index.dart';
import 'package:chat_app/src/pages/Chat/components/NavigationBar/constants/index.dart';
import 'package:chat_app/src/pages/Chat/models/SizeActionData.dart';
import 'package:chat_app/src/pages/Chat/utils/index.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

import 'components/CloseButtonNavBar/index.dart';
import 'components/NavTitle/index.dart';

class NavigationBar extends StatefulWidget {
  final Function handleToogleHeight;
  final bool isOpen;

  NavigationBar({
    Key key,
    @required this.handleToogleHeight,
    @required this.isOpen,
  }) : super(key: key);

  _NavigationBarState createState() => new _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  SizeActionData iconSize;

  @override
  void initState() {
    iconSize = getInitialActionSize(30.0);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant NavigationBar oldWidget) {
    setState(() {
      if (widget.isOpen) {
        iconSize = actionSmallToLargetUserIconSize;
        return;
      }

      iconSize = actionLargeToSmalltUserIconSize;
    });

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.handleToogleHeight,
      child: SafeArea(
        top: true,
        child: Container(
          color: getColorFromHEX('#2040FF'),
          child: FractionallySizedBox(
              widthFactor: 1,
              heightFactor: 1,
              child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
                  child: Column(
                    children: [
                      TweenAnimationBuilder(
                          tween: Tween<double>(
                              begin: iconSize.begin, end: iconSize.end),
                          duration: Duration(microseconds: 300),
                          builder: (context, double iconSizeData, __) {
                            return Container(
                              height: iconSizeData + 20,
                              child: Row(
                                children: [
                                  UserIcon(iconSize: iconSizeData),
                                  NavTitle(isOpen: widget.isOpen),
                                  Container(
                                      alignment: Alignment.topRight,
                                      padding: EdgeInsets.only(top: 14),
                                      child: CloseButtonNavBar())
                                ],
                              ),
                            );
                          }),
                      InfoTitle(isOpen: widget.isOpen)
                    ],
                  ))),
        ),
      ),
    );
  }
}
