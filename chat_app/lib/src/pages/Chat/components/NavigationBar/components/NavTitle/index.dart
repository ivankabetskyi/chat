import 'package:chat_app/src/pages/Chat/components/ChatMessage/utils/index.dart';
import 'package:chat_app/src/pages/Chat/data/index.dart';
import 'package:flutter/material.dart';

class NavTitle extends StatelessWidget {
  final bool isOpen;
  NavTitle({Key key, this.isOpen}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: !isOpen
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    getFullUserName(currentUser),
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  Text(
                    'online',
                    style: TextStyle(color: Colors.white54, fontSize: 14),
                  ),
                ],
              )
            : SizedBox(),
      ),
    );
  }
}
