import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

class UserIcon extends StatelessWidget {
  final double iconSize;
  UserIcon({Key key, @required this.iconSize}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: iconSize,
      height: iconSize,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
        child: Icon(
          Icons.forum_outlined,
          color: getColorFromHEX('#2040FF'),
          size: iconSize * .8,
        ),
      ),
    );
  }
}
