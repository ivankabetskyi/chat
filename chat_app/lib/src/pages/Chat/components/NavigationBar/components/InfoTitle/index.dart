import 'package:flutter/material.dart';

class InfoTitle extends StatelessWidget {
  final bool isOpen;
  InfoTitle({Key key, this.isOpen}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return isOpen
        ? Expanded(
            child: Container(
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Hi there!',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold)),
                Text('Welcome to Workspace Go. How can we help you today?',
                    style: TextStyle(color: Colors.white))
              ],
            ),
          ))
        : SizedBox();
  }
}
