import 'package:chat_app/src/pages/Chat/components/common/ActionButton/index.dart';
import 'package:flutter/material.dart';

class CloseButtonNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      height: 20,
      child: ActionButton(
        icon: Icons.close,
        iconColor: Colors.white,
        size: 24.0,
      ),
    );
  }
}
