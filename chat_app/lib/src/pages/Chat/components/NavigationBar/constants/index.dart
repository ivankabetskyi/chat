import 'package:chat_app/src/pages/Chat/models/SizeActionData.dart';

SizeActionData actionSmallToLargetUserIconSize =
    new SizeActionData(begin: 30.0, end: 50.0);
SizeActionData actionLargeToSmalltUserIconSize =
    new SizeActionData(begin: 50.0, end: 30.0);
