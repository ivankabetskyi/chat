import 'package:chat_app/src/app/models/UserData/index.dart';

class MessageData {
  UserData sender;
  String message;
  DateTime createdAt;

  MessageData({this.sender, this.message, this.createdAt});
}
