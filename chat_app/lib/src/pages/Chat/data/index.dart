import 'package:chat_app/src/app/models/UserData/index.dart';
import 'package:chat_app/src/pages/Chat/models/MessageData.dart';
import 'package:flutter/material.dart';

UserData user = new UserData(
    id: 0,
    name: 'Gena',
    surname: 'Bukin',
    isOnline: true,
    avatar: Icons.account_circle);

UserData currentUser = new UserData(
    id: 1,
    name: 'Roma',
    surname: 'Bukin',
    isOnline: true,
    avatar: Icons.account_circle);

List<MessageData> messagesData = [
  new MessageData(
    sender: user,
    message:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sollicitudin.',
    createdAt: new DateTime.now(),
  ),
  new MessageData(
    sender: currentUser,
    message:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sollicitudin.',
    createdAt: new DateTime.now(),
  )
];
