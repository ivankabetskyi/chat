import 'package:chat_app/src/app/models/UserData/index.dart';
import 'package:chat_app/src/pages/Chat/models/SizeActionData.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

SizeActionData getInitialActionSize(double size) {
  return new SizeActionData(begin: size, end: size);
}

BorderRadius getBorderRadiusForMessage(UserData sender, UserData currentUser) {
  bool isCurrentUser = isCurrentUserMessage(sender, currentUser);
  return BorderRadius.only(
      topLeft: Radius.circular(isCurrentUser ? 40.0 : 4.0),
      topRight: Radius.circular(isCurrentUser ? 4 : 40.0),
      bottomLeft: Radius.circular(40.0),
      bottomRight: Radius.circular(40.0));
}

MainAxisAlignment getAligmentForMessagge(
    UserData sender, UserData currentUser) {
  return isCurrentUserMessage(sender, currentUser)
      ? MainAxisAlignment.end
      : MainAxisAlignment.start;
}

Color getColorForMessage(UserData sender, UserData currentUser) {
  return isCurrentUserMessage(sender, currentUser)
      ? getColorFromHEX('#2040FF')
      : getColorFromHEX('#F5F6FA');
}

Color getTextColorForMessage(UserData sender, UserData currentUser) {
  return isCurrentUserMessage(sender, currentUser)
      ? Colors.white
      : Colors.black87;
}

bool isCurrentUserMessage(UserData sender, UserData currentUser) =>
    sender.id == currentUser.id;
