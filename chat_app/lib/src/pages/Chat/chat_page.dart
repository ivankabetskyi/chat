import 'package:chat_app/src/pages/Chat/components/NavigationBar/index.dart';
import 'package:chat_app/src/pages/Chat/components/SendComponent/index.dart';
import 'package:chat_app/src/pages/Chat/data/index.dart';
import 'package:chat_app/src/pages/Chat/models/MessageData.dart';
import 'package:chat_app/src/pages/Chat/utils/index.dart';
import 'package:chat_app/src/utils/index.dart';
import 'package:flutter/material.dart';

import 'components/ChatMessage/index.dart';

class ChatPage extends StatefulWidget {
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<ChatPage> with TickerProviderStateMixin {
  bool isOpen;
  AnimationController _controllerAppBarHeightAnimation;

  Animation<Size> appBarHeightAnimation;

  @override
  void initState() {
    isOpen = false;
    _controllerAppBarHeightAnimation = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );

    appBarHeightAnimation =
        Tween<Size>(begin: Size.fromHeight(80), end: Size.fromHeight(170))
            .animate(_controllerAppBarHeightAnimation)
              ..addListener(() {
                setState(() {});
              });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: appBarHeightAnimation.value,
            child: NavigationBar(
              handleToogleHeight: handleNavigationHeight,
              isOpen: isOpen,
            )),
        body: GestureDetector(
          onTap: () => clickOutsideTextFeild(context),
          child: Container(
              color: getColorFromHEX('#FFFFFF'),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 10.0, left: 14, right: 14.0, bottom: 2.0),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView(
                        children: getMessageWidget(),
                      ),
                    ),
                    SendComponent(),
                  ],
                ),
              )),
        ));
  }

  List<ChatMessage> getMessageWidget() {
    return messagesData
        .map((MessageData messageData) => ChatMessage(
            message: messageData,
            borderRadius:
                getBorderRadiusForMessage(messageData.sender, currentUser),
            mainAxisAlignment:
                getAligmentForMessagge(messageData.sender, currentUser),
            backgroundColor:
                getColorForMessage(messageData.sender, currentUser),
            textColor: getTextColorForMessage(messageData.sender, currentUser),
            isCurrentUser:
                isCurrentUserMessage(messageData.sender, currentUser)))
        .toList();
  }

  void handleNavigationHeight() {
    setState(() {
      clickOutsideTextFeild(context);
      if (!isOpen) {
        _controllerAppBarHeightAnimation.forward();
      } else {
        _controllerAppBarHeightAnimation.reverse();
      }
      isOpen = !isOpen;
    });
  }
}
