import 'package:flutter/material.dart';

class UserData {
  int id;
  String name;
  String surname;
  IconData avatar;
  bool isOnline;

  UserData({this.avatar, this.name, this.surname, this.isOnline, this.id});
}
