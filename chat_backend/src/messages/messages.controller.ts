import {
  Controller,
  Get,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';

// import { CreateCatDto } from './dto/create-cat.dto';
import { MessagesService } from './messages.service';
import { Message } from './interfaces/message.interface';

@Controller('messages')
export class MessagesController {
  constructor(private messagesService: MessagesService) {}

  // @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  async findAll(): Promise<any> {
    const allMessages = await this.messagesService.findAll();
    return allMessages;
  }
}
