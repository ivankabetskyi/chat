export interface Message {
  id: string;
  roomId: string;
  text: string;
  createdAt: any;
  updatedAt: any;
}
