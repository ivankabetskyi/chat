import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { MessageDocument, Message } from './schemas/message.schema';
// import { Bid } from './interfaces/bid.interface';
// import { CreateCatDto } from './dto/create-cat.dto';

@Injectable()
export class MessagesService {
  constructor(
    @InjectModel(Message.name) private messageModel: Model<MessageDocument>,
  ) {}

  // async create(createCatDto: CreateCatDto): Promise<Cat> {
  //     const createdCat = new this.catModel(createCatDto);
  //     return createdCat.save();
  // }

  async findAll(): Promise<Message[]> {
    return this.messageModel.find().exec();
  }
}
