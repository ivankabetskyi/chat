import { Module } from '@nestjs/common';
import { MessageController } from './infrastructure/controllers/message.controller';
import { MessagesRepositoriesModule } from './infrastructure/repositories/repositories.module';
import { providers } from './infrastructure/providers';

@Module({
  controllers: [MessageController],
  imports: [MessagesRepositoriesModule],
  providers: [...providers],
})
export class CleanArchitectureModule {}
