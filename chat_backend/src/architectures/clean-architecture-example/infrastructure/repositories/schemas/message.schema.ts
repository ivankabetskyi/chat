import { raw, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type MessageDocument = Message & Document;

@Schema()
export class Message {
  @Prop({ type: Types.ObjectId })
  id: string;

  @Prop({ index: true, required: true })
  roomId: string;

  @Prop(
    raw({
      firstName: { type: String },
      lastName: { type: String },
    }),
  )
  sender: Record<string, any>;

  @Prop(
    raw({
      firstName: { type: String },
      lastName: { type: String },
    }),
  )
  receiver: Record<string, any>;

  @Prop({ required: true })
  text: string;

  @Prop({ index: true, required: true })
  created_at: Date;

  @Prop({ index: true })
  updated_at: Date;
}

export const MessageSchema = SchemaFactory.createForClass(Message);
