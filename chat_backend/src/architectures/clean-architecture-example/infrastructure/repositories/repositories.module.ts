import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Message, MessageSchema } from './schemas/message.schema';
import { MessageRepository } from './message.repository';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Message.name, schema: MessageSchema }]),
  ],
  providers: [MessageRepository],
  exports: [
    MongooseModule.forFeature([{ name: Message.name, schema: MessageSchema }]),
    MessageRepository,
  ],
})
export class MessagesRepositoriesModule {}
