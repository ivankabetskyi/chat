import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { MessageEntity } from '../../domain/message';
import { IMessageRepository } from '../../domain/types/message.repository';
import { CreateMessageDto } from '../../domain/types/create-message-dto';
import { MessageDocument, Message } from './schemas/message.schema';

@Injectable()
export class MessageRepository implements IMessageRepository {
  constructor(
    @InjectModel(Message.name)
    private messageModel: Model<MessageDocument>,
  ) {}

  async findAll(): Promise<MessageEntity[]> {
    const messagesDocuments: Message[] = await this.messageModel.find().exec();
    return messagesDocuments.map(this.toEntityFromDoc);
  }

  async create(createMessageDto: CreateMessageDto): Promise<any> {
    const dataForCreate = {
      ...createMessageDto,
      created_at: new Date(),
      updated_at: null,
    };
    console.info("dasdasdasdasd#####", dataForCreate);
    const createdMessageDoc: Message = await this.messageModel.create(
      dataForCreate,
    );
    return this.toEntityFromDoc(createdMessageDoc);
  }

  private toEntityFromDoc(messageDoc: Message): MessageEntity {
    const message: MessageEntity = new MessageEntity({
      id: messageDoc.id,
      text: messageDoc.text,
      roomId: messageDoc.roomId,
      sender: messageDoc.sender as { firstName: string; lastName: string },
      receiver: messageDoc.receiver as {
        firstName: string;
        lastName: string;
      },
      createdAt: messageDoc.created_at,
      updatedAt: messageDoc.updated_at,
    });
    return message;
  }

  private toEntityFromDTO(messageDoc: Message): MessageEntity {
    const message: MessageEntity = new MessageEntity({
      id: messageDoc.id,
      text: messageDoc.text,
      roomId: messageDoc.roomId,
      sender: messageDoc.sender as { firstName: string; lastName: string },
      receiver: messageDoc.receiver as {
        firstName: string;
        lastName: string;
      },
      createdAt: messageDoc.created_at,
      updatedAt: messageDoc.updated_at,
    });
    return message;
  }
}
