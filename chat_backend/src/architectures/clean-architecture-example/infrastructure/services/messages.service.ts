import { Injectable } from '@nestjs/common';

import { GetAllMessages } from '../../domain/use-cases/get-all-messages';
import { CreateNewMessage } from '../../domain/use-cases/create-new-message';
import { CreateMessageDto } from '../../domain/types/create-message-dto';
import { MessageRepository } from '../repositories/message.repository';

@Injectable()
export class MessagesService {
  constructor(
    private getAllMessagesUseCase: GetAllMessages,
    private createNewMessageUseCase: CreateNewMessage,
    private messagesRepository: MessageRepository,
  ) {}

  async getAll(): Promise<ReturnType<GetAllMessages['execute']>> {
    return this.getAllMessagesUseCase.execute(this.messagesRepository);
  }

  async create(
    dto: CreateMessageDto,
  ): Promise<ReturnType<CreateNewMessage['execute']>> {
    return this.createNewMessageUseCase.execute(dto, this.messagesRepository);
  }
}
