import { Body, Controller, Get, Post } from '@nestjs/common';
import { MessageEntity } from '../../domain/message';
import { CreateMessageDto } from '../../domain/types/create-message-dto';
import { MessagesService } from '../services/messages.service';

@Controller('/messages')
export class MessageController {
  constructor(private readonly messageService: MessagesService) {}

  @Get()
  async getAllMessages(): Promise<MessageEntity[]> {
    return this.messageService.getAll();
  }

  @Post()
  async postMessage(
    @Body() postMessageData: CreateMessageDto,
  ): Promise<MessageEntity> {
    console.info("DTOOOO!!!!!!!!!!!!!!!!!!1", postMessageData);
    return this.messageService.create(postMessageData);
  }
}
