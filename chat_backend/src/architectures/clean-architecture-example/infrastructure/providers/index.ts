import { Provider } from '@nestjs/common';

import { GetAllMessages } from '../../domain/use-cases/get-all-messages';
import { CreateNewMessage } from '../../domain/use-cases/create-new-message';

import { MessagesService } from '../services/messages.service';

export const providers: Provider[] = [
  MessagesService,
  {
    provide: GetAllMessages,
    useClass: GetAllMessages,
  },
  {
    provide: CreateNewMessage,
    useClass: CreateNewMessage,
  },
];
