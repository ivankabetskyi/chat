import { MessageEntity } from '../message';
import { CreateMessageDto } from './create-message-dto';

export interface IMessageRepository {
  create(dto: CreateMessageDto): Promise<MessageEntity>;
  findAll(): Promise<MessageEntity[]>;
}
