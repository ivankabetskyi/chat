export class CreateMessageDto {
  text: string;
  roomId: string;
  sender: { firstName: string; lastName: string };
  receiver: { firstName: string; lastName: string };
}
