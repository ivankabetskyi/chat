import { isEmpty } from 'lodash';

type User = {
  firstName: string;
  lastName: string;
};

type MessageProps = {
  id: string;
  text: string;
  roomId: string;
  sender: User;
  receiver: User;
  createdAt: Date;
  updatedAt: Date;
};

export class MessageEntity {
  id: string;
  text: string;
  roomId: string;
  sender: User;
  receiver: User;
  createdAt: Date;
  updatedAt: Date;

  constructor(props: MessageProps) {
    if (isEmpty(props.text)) {
      throw new Error('text cannot be null or empty');
    }
    this.id = props.id;
    this.text = props.text;
    this.roomId = props.roomId;
    this.sender = props.sender;
    this.receiver = props.receiver;
    this.createdAt = props.createdAt;
    this.updatedAt = props.updatedAt;
  }
}
