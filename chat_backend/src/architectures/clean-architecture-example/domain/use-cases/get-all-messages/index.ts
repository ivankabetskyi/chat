import { MessageEntity } from '../../message';
import { IMessageRepository } from '../../types/message.repository';

export class GetAllMessages {
  async execute(
    messageRepository: IMessageRepository,
  ): Promise<MessageEntity[]> {
    return messageRepository.findAll();
  }
}
