import { MessageEntity } from '../../message';
import { IMessageRepository } from '../../types/message.repository';
import { CreateMessageDto } from '../../types/create-message-dto';

export class CreateNewMessage {
  async execute(
    dto: CreateMessageDto,
    messageRepository: IMessageRepository,
  ): Promise<MessageEntity> {
    return messageRepository.create(dto);
  }
}
