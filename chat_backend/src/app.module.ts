/*import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { MessagesModule } from './messages/messages.module';
import { DB_URL } from './config';

@Module({
  imports: [MongooseModule.forRoot(DB_URL), MessagesModule],
})
export class AppModule {}*/

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DB_URL } from './config';
import { CleanArchitectureModule } from './architectures/clean-architecture-example/main';

@Module({
  imports: [MongooseModule.forRoot(DB_URL), CleanArchitectureModule],
})
export class AppModule {}
